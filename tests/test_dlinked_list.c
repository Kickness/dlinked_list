#include <unitest.h>
#include <time.h>
#include <stdio.h>
#include <malloc.h>

#include <dlinked_list.h>

long size = 2000000, from = 5732, to = 6500;

void
test_push_pop(){
    dl_node_t* ptr;
    double f = 3.14, *o, total = 0.0;
    dl_list_t dl;

    dl_init(&dl);

    dl_push(&dl, &f);
    T_ASSERT(dl.head == dl.end);
    o = dl_pop(&dl);
    T_ASSERT_FLOAT(*o, 3.14);
    dl_push(&dl, &f);
    dl_push(&dl, &f);
    dl_push(&dl, &f);
    T_ASSERT(dl.head != dl.end);
    T_ASSERT_NUM(dl.size, 3);
    /** Iterate through elements */
    ptr = dl.head;
    do{
        o = ptr->data;
        total += *o;
    }while( (ptr = ptr->next) );
    T_ASSERT_FLOAT(total, 9.42);

    /* Clean up */
    while(dl_pop(&dl));
    T_ASSERT_NUM(dl.size, 0);
    T_ASSERT(!dl.head);
    T_ASSERT(!dl.end);
}

void
test_pop_head(){
    dl_node_t* n2, *n4;
    double f = 3.14;
    dl_list_t dl;

    dl_init(&dl);

    dl_push(&dl, &f);
    n2 = dl_push(&dl, &f);
    dl_push(&dl, &f);
    dl_push(&dl, &f);
    T_ASSERT(dl.head != dl.end);
    T_ASSERT_NUM(dl.size, 4);

    dl_pop_head(&dl);
    n4 = dl_push(&dl, &f);
    dl_pop(&dl);
    T_ASSERT(dl.head = n2);
    T_ASSERT(dl.end = n4);
    T_ASSERT_NUM(dl.size, 3);

    while(dl_pop_head(&dl));
    T_ASSERT_NUM(dl.size, 0);
    T_ASSERT(!dl.head);
    T_ASSERT(!dl.end);
}

void
unlinking(){
    int count;
    dl_list_t* dl;
    dl_node_t* mid, *head, *end, *n2, *n4, *ptr;

    dl = malloc(sizeof(*dl));
    dl_init(dl);
    head = dl_push(dl, (void*)1);
    n2 = dl_push(dl, (void*)2);
    mid = dl_push(dl, (void*)3);
    n4 = dl_push(dl, (void*)4);
    end = dl_push(dl, (void*)5);

    T_ASSERT_NUM(dl->size, 5);
    dl_unlink(dl, mid);
    T_ASSERT_NUM(dl->size, 4);

    /* All nodes are connected */
    count = 0;
    ptr = dl->head;
    while( ptr->next && (ptr = ptr->next) ) count++;
    T_ASSERT(ptr == end);
    T_ASSERT_NUM(count + 1, 4);

    dl_unlink(dl, head);
    T_ASSERT_NUM(dl->size, 3);
    T_ASSERT(dl->head == n2);

    dl_unlink(dl, end);
    T_ASSERT_NUM(dl->size, 2);
    T_ASSERT(dl->end == n4);

    /* All nodes are connected */
    count = 0;
    ptr = dl->head;
    while( ptr->next && (ptr = ptr->next) ) count++;
    T_ASSERT(ptr == n4);
    T_ASSERT_NUM(count + 1, 2);

    /* Clean up */
    while(dl_pop(dl));
    T_ASSERT_NUM(dl->size, 0);
    T_ASSERT(!dl->head);
    T_ASSERT(!dl->end);
    free(dl);
}

dl_node_t*
find_item_list(dl_list_t* dl, long n){
    dl_node_t* ptr;
    ptr = dl->head;
    while( ptr && ptr->data != (void*)n ){ ptr = ptr->next; }
    return ptr;
}

void*
remove_item_list(dl_list_t* dl, long n){
    dl_node_t* ptr;

    ptr = find_item_list(dl, n);
    return dl_unlink(dl, ptr);
}

void
stress_linked_list(){
    long i = 0;
    clock_t start, end;
    dl_list_t dl;
    dl_init(&dl);

    start = clock();
    i = 0; while(i++ < size) dl_push(&dl, (void*)i);
    end = clock();
    printf("List assign took: %f seconds\n", (double)(end - start) / CLOCKS_PER_SEC);

    start = clock();
    i = from; while(i++ < to){
        long value = (long)remove_item_list(&dl, i);
        T_ASSERT_NUM(value, i);
    }
    end = clock();
    printf("List removal took: %f seconds\n", (double)(end - start) / CLOCKS_PER_SEC);

    while( dl_pop(&dl) );
}

long*
find_item_arr(long* arr, long size, long n){
    long* ptr = arr;

    while(*ptr && *ptr != n) ptr++;
    return ptr;
}

long
remove_item_arr(long* arr, long size, int n){
    long value;
    long* ptr;

    ptr = find_item_arr(arr, size, n);
    value = *ptr;

    while( ptr[1] ){
        *ptr = ptr[1];
        ptr++;
    }
    *ptr = ptr[1];
    return value;
}

void
stress_array(){
    long i = 0;
    clock_t start, end;
    long* array = malloc(sizeof(long) * (size + 1));
    array[size] = 0;

    start = clock();
    i = 0; while(i++ < size) array[i-1] = i;
    end = clock();
    printf("Array assign took: %f seconds\n", (double)(end - start) / CLOCKS_PER_SEC);

    start = clock();
    i = from; while(i++ < to)
        find_item_arr(array, size, i);
    end = clock();
    printf("Array removal took: %f seconds\n", (double)(end - start) / CLOCKS_PER_SEC);

    free(array);
}

int main(void){
    T_SUITE(Double Linked List,
        TEST(Push and pop, test_push_pop());
        TEST(Unlinking, unlinking());
        TEST(Pop head to tail, test_pop_head());
    );

    TEST(Stress test linked list, stress_linked_list())

    TEST(Stress test array, stress_array())

    T_CONCLUDE();
    return 0;
}
